import { createStore } from "vuex";
// import actions from './actions.js'
// import mutations from "./mutations.js";
// import getters from "./getters.js";

import productStore from '@/modules/products/store/index.js'

// const moduleA = {
//   productStore
// }

const store = createStore({
  // modules: {
  //   products: moduleA,
  // }
  // actions,
  // mutations,
  // getters
  productStore
});
export default store;