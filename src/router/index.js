import { createWebHistory, createRouter } from "vue-router";
const HomePage = () => import('@/components/main/HomePage1.vue');
const LoginPage = () => import('@/components/main/LoginPage.vue');
const ServicePage = () => import('@/components/main/ServicePage.vue');
const ProfilePage = () => import('@/components/main/ProfilePage.vue');
// import GeneralRoutes from "./router.js";
// import PropertyRoutes from "./property.js";

// var allRoutes = [];
// // allRoutes = allRoutes.concat(GeneralRoutes, PropertyRoutes);
// allRoutes = allRoutes.concat(GeneralRoutes);

// const routes = allRoutes;

const routes = [
  {
    path: "/",
    name: "Home-page",
    component: HomePage
  },
  {
    path: "/login",
    name: "Login-page",
    component: LoginPage
  },
  {
    path: "/service",
    name: "Service-page",
    component: ServicePage
  },
  {
    path: "/profile",
    name: "profile-page",
    component: ProfilePage
  }
];

// const needsAuth = false;

const router = createRouter({
  history: createWebHistory(),
  routes,
});

// router.beforeEach((to, from, next) => {
//   if(to.meta.requiresAuth && !needsAuth){
//     next({ name: 'Test' })
//     // return {
//     //   path: '/',
//     // }
//   }
//   next();
// });

// console.log(router);

export default router;   