import Axios from 'axios';
const RESOURCE_NAME = '/api/user/home/products';

export default{
      getAll() {
        return Axios.get(RESOURCE_NAME);
      },      
}