// import router from '@/router';

const CartModule = () => import('./CartModule.vue');
const CartPage = () => import('./views/CartPage.vue');
const CheckoutPage = () => import('./views/CheckoutPage.vue');

const moduleRoute ={
    path:'/cart',
    component: CartModule,
    children:[
        {
            path:"/cart",
            component: CartPage
        },
        {
            path:"/checkout",
            component: CheckoutPage
        }
    ]
}

export default router => {
    router.addRoute(moduleRoute);
  };
  