const CheckModule = () => import('./CheckModule.vue');
const HomePage = () => import('./views/HomePage.vue');
const DashboardPage = () => import('./views/DashboardPage.vue');
const ErrorPage = () => import('./views/404Page.vue');
const CheckPage = () => import('./views/CheckPage.vue');
const HomeCirclePage = () => import('./views/HomeCirclePage.vue');
const AsidePage = () => import('./views/AsidePage.vue');
const ChatPage = () => import('./views/ChatPage.vue');
const TestPage = () => import('./views/TestPage.vue');
const CheckTextPage = () => import('./views/CheckTextPage.vue');
const CheckInputPage = () => import('./views/CheckInputPage.vue');
const CheckSelectPage = () => import('./views/CheckSelectPage.vue');
const CheckButtonPage = () => import('./views/CheckButtonPage.vue');
const CheckLoginRegister = () => import('./views/CheckLoginRegister.vue');
const NotificationPage = () => import('./views/NotificationPage.vue');
const CheckLoadingPage = () => import('./views/CheckLoadingPage.vue');
const CheckServicePage = () => import('./views/CheckServicePage.vue');
const CheckMultiFormPage = () => import('./views/CheckMultiFormPage.vue');
const CheckFormPage = () => import('./views/comps/form/CheckFormPage.vue');
const CheckCreateForm = () => import('./views/comps/multiform/CreateForm.vue');
const CheckInvoicePage = () => import('./views/CheckInvoicePage.vue');
const CheckPopupPage = () => import('./views/CheckPopupPage.vue');
const AccordionPage = () => import('./views/AccordionPage.vue');
const LocalhostPage = () => import('./views/LocalhostPage.vue');
const LocalPdf = () => import('./views/comps/localhost/LocalPdf.vue');
const BalanceWithdraw = () => import('./views/BalanceWithdraw.vue');
const BlogPage = () => import('./views/comps/blog/BlogPage.vue');
const BlogDetailsPage = () => import('./views/comps/blog/BlogDetailsPage.vue');
const PaginationPage = () => import('./views/PaginationPage.vue');
const QrCodePage = () => import('./views/QrCodePage.vue');
const SlidePage = () => import('./views/SlidePage.vue');
const DoctorForm = () => import('./views/DoctorForm.vue');
const DoctorProfile = () => import('./views/comps/doctor/DoctorProfile.vue');
const DoctorProfile2 = () => import('./views/comps/doctor/DoctorProfile2.vue');
const ShopItems = () => import('./views/comps/shop/ShopItem.vue');



const moduleRoute = {
    path: "/check",
    component: CheckModule,

    children:[
        {
            path: "/check",
            component: CheckPage,
        },
        {
            path: "/check/home-page",
            component: HomePage,
        },
        {
            path: "/check/dashboard",
            component: DashboardPage,
        },
        {
            path: "/check/404page",
            component: ErrorPage,
        },
        {
            path: "/check/home-circle",
            component: HomeCirclePage,
        },
        {
            path: "/check/aside",
            component: AsidePage,
        },
        {
            path: "/check/chat",
            component: ChatPage,
        },
        {
            path: "/check/test",
            component: TestPage,
        },
        {
            path: "/check/text",
            component: CheckTextPage,
        },
        {
            path: "/check/input",
            component: CheckInputPage,
        },
        {
            path: "/check/select",
            component: CheckSelectPage,
        },
        {
            path: "/check/button",
            component: CheckButtonPage,
        },
        {
            path: "/check/log-reg",
            component: CheckLoginRegister,
        },
        {
            path: "/check/noti",
            component: NotificationPage,
        },
        {
            path: "/check/loader",
            component: CheckLoadingPage,
        },
        {
            path: "/check/service",
            component: CheckServicePage,
        },
        {
            path: "/check/form",
            component: CheckFormPage,
        },
        {
            path: "/check/multi-form",
            component: CheckMultiFormPage,
        },
        {
            path: "/check/create-form",
            component: CheckCreateForm,
        },
        {
            path: "/check/invoice",
            component: CheckInvoicePage,
        },
        {
            path: "/check/popup",
            component: CheckPopupPage,
        },
        {
            path: "/check/accordion",
            component: AccordionPage,
        },
        {
            path: "/check/localhost",
            component: LocalhostPage,
        },
        {
            path: "/check/localhost/pdf",
            component: LocalPdf,
        },
        {
            path: "/check/balance-withdraw",
            component: BalanceWithdraw,
        },
        {
            path: "/check/blog",
            component: BlogPage,
        },
        {
            path: "/check/blog/details",
            component: BlogDetailsPage,
        },
        {
            path: "/check/pagination",
            component: PaginationPage,
        },
        {
            path: "/check/qr-code",
            component: QrCodePage,
        },
        {
            path: "/check/slide",
            component: SlidePage,
        },
        {
            path: "/check/doctor",
            component: DoctorForm,
        },
        {
            path: "/check/doctor-profile",
            component: DoctorProfile,
        },
        {
            path: "/check/doctor-profile-2",
            component: DoctorProfile2,
        },
        {
            path: "/check/shops",
            component: ShopItems,
        },
    ]
}

export default router => {
    router.addRoute(moduleRoute);
}