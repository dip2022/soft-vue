const ShopModule = () => import('./ShopModule.vue');
const ShopPage = () => import('./views/ShopPage.vue');
const ShopPage1 = () => import('./views/ShopPage1.vue');


const moduleRoute = {
    path: "/shop",
    component: ShopModule,

    children:[
        {
            path: "/shop",
            component: ShopPage,
        },
        {
            path: "/shop1",
            component: ShopPage1,
        }
    ]
}

export default router => {
    router.addRoute(moduleRoute);
}