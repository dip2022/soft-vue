// import router from '@/router';

const BalanceModules = () => import('./BalanceModules.vue');
const BalancePage = () => import('./page/BalancePage.vue');

const moduleRoute = {
    path: "/admin/balance",
    component: BalanceModules,
  
    children:[
        {
            path: "/admin/balance",
            component: BalancePage,
        },
    ]
};
export default router => {
    router.addRoute(moduleRoute);
    // console.log(router.getRoutes());
  };
  