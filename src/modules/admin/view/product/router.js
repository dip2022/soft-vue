// import router from '@/router';

const ProductModules = () => import('./ProductModules.vue');
const ProductsPage = () => import('./page/ProductsPage.vue');
const ProductsAddPage = () => import('./page/ProductsAddPage.vue');
const BrandPage = () => import('./brand/BrandPage.vue');
const BrandAddPage = () => import('./brand/BrandAddPage.vue');
const BrandEditPage = () => import('./brand/BrandEditPage.vue');
const CategoryPage = () => import('./categories/CategoryPage.vue');
const CategoryAddPage = () => import('./categories/CategoryAddPage.vue');
const CategoryEditPage = () => import('./categories/CategoryEditPage.vue');
const SubCategoryPage = () => import('./subcategories/SubCategoryPage.vue');
const SubCategoryAddPage = () => import('./subcategories/SubCategoryAddPage.vue');
const SubCategoryEditPage = () => import('./subcategories/SubCategoryEditPage.vue');

const moduleRoute = {
    path: "/admin/products",
    component: ProductModules,
  
    children:[
        {
            path: "/admin/products",
            component: ProductsPage,
        },
        {
            path: "/admin/products/add",
            component: ProductsAddPage,
        },
        {
            path: "/admin/products/brand",
            component: BrandPage,
        },
        {
            path: "/admin/products/brand-add",
            component: BrandAddPage,
        },
        {
            path: "/admin/products/brand-edit",
            component: BrandEditPage,
        },
        {
            path: "/admin/products/category",
            component: CategoryPage,
        },
        {
            path: "/admin/products/category-add",
            component: CategoryAddPage,
        },
        {
            path: "/admin/products/category-edit",
            component: CategoryEditPage,
        },
        {
            path: "/admin/products/sub-catetory",
            component: SubCategoryPage,
        },
        {
            path: "/admin/products/sub-catetory-add",
            component: SubCategoryAddPage,
        },
        {
            path: "/admin/products/sub-catetory-edit",
            component: SubCategoryEditPage,
        },
    ]
};
export default router => {
    router.addRoute(moduleRoute);
    // console.log(router.getRoutes());
  };
  