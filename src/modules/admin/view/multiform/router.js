// import router from '@/router';

const MultiFormModules = () => import('./MultiFormModules.vue');
const MultiFormPage = () => import('./page/MultiFormPage.vue');

const moduleRoute = {
    path: "/admin/multi-form",
    component: MultiFormModules,
  
    children:[
        {
            path: "/admin/multi-form",
            component: MultiFormPage,
        },
    ]
};
export default router => {
    router.addRoute(moduleRoute);
    // console.log(router.getRoutes());
  };
  