// import router from '@/router';

const ServiceModules = () => import('./ServiceModules.vue');
const ServicePage = () => import('./page/ServicePage.vue');
const ServiceCreatePage = () => import('./page/ServiceCreatePage.vue');
const ServiceProfileEditPage = () => import('./page/ServiceProfileEditPage.vue');

const moduleRoute = {
    path: "/mypanel/service",
    component: ServiceModules,
  
    children:[
        {
            path: "/mypanel/service",
            component: ServicePage,
        },
        {
            path: "/mypanel/service/create",
            component: ServiceCreatePage,
        },
        {
            path: "/mypanel/service/edit",
            component: ServiceProfileEditPage,
        },
    ]
};
export default router => {
    router.addRoute(moduleRoute);
    // console.log(router.getRoutes());
  };
  