import { createApp } from 'vue'
import App from './App.vue'

import axios from 'axios'
import VueAxios from 'vue-axios'

import upperFirst from "lodash/upperFirst";
import camelCase from "lodash/camelCase";
import router from './router/index.js';
import store from './store/index.js'
//Fronted module
import cartModule from './modules/cart/index.js';
import productsModule from './modules/products/index.js';
import checkModule from './modules/check/index.js';
import shopModule from './modules/shop/index.js';

//Admin module
import AdminModule from './modules/admin/index.js';
import AdminServiceModule from './modules/admin/view/services/index.js';
import AdminUserModule from './modules/admin/view/user/index.js';
import AdminWorkStationModule from './modules/admin/view/workstation/index.js';
import AdminCategoriesModule from './modules/admin/view/category/index.js';
import AdminProductModule from './modules/admin/view/product/index.js';
import AdminMultiFormModule from './modules/admin/view/multiform/index.js';
import AdminBalanceModule from './modules/admin/view/balance/index.js';


//User module
import UserModule from './modules/user/index.js';
import UserServiceModule from './modules/user/view/service/index.js';


import { registerModules } from './register-modules';
registerModules({
  products: productsModule,
  cart: cartModule,
  check: checkModule,
  shop: shopModule,

  // Admin
  admin: AdminModule,
  adminservice:AdminServiceModule,
  adminuser:AdminUserModule,
  adminworkstation:AdminWorkStationModule,
  admincategories:AdminCategoriesModule,
  adminproduct:AdminProductModule,
  adminfomr:AdminMultiFormModule,
  adminbalance:AdminBalanceModule,

  // User
  user: UserModule,
  userservice: UserServiceModule,
});




const requireComponent = require.context(
    "./layout/base/",
    false,
    /^\.\/.*$/,
    // /base-[\w-]+\.vue$/,
    "sync"
  );

axios.defaults.headers.common['Access-Control-Allow-Origin'] = '*';
axios.defaults.baseURL = process.env.VUE_APP_API_URL;


const app = createApp(App);
app.use(router);
app.use(VueAxios, axios);
app.use(store);
  requireComponent.keys().forEach((fileName) => {
    const componentConfig = requireComponent(fileName);
  
    // const componentName = upperFirst(
    //   camelCase(fileName.replace(/^\.\//, "").replace(/\.\w+$/, ""))
    // );

    const componentName = upperFirst(
        camelCase(fileName.replace(/^\.\//, ""))
      );
    app.component(componentName, componentConfig.default || componentConfig);
  });


app.mount('#app');
